#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define NVEL 9

void macroscopicQuantities(int NX, int NY, float rho[NX][NY], float u[NX][NY], float v[NX][NY], float fprop[NX][NY][NVEL], int cx[NVEL], int cy[NVEL], float Fx, float Fy, float dt){
  int i = 0, j = 0, k = 0;
  float soma = 0., somau = 0., somav = 0.;
  for(j=0;j<NY;j++){
    for(i=0;i<NX;i++){
      soma = 0.0;
      for(k=0;k<NVEL;k++){
        soma = soma + fprop[i][j][k];
      }
      rho[i][j] = soma; //densidade
    }
  }

  for(j=0;j<NY;j++){
    for(i=0;i<NX;i++){
      somau = 0.0;
      somav = 0.0;
      for(k=0;k<NVEL;k++){
          somau = somau + fprop[i][j][k] * cx[k];
          somav = somav + fprop[i][j][k] * cy[k];
      }
      u[i][j] = (somau + 0.5 * Fx * dt) / rho[i][j]; //componente x da velocidade
      v[i][j] = (somav + 0.5 * Fy * dt) / rho[i][j]; //componente y da velocidade 
    }
  }
}

void collisionFineGrid(int NX, int NY, float tau, float rho[NX][NY], float u[NX][NY], float v[NX][NY],float f[NX][NY][NVEL], float fprop[NX][NY][NVEL], float feq[NX][NY][NVEL], float F[NX][NY][NVEL], int cx[NVEL], int cy[NVEL], float w[NVEL], float Fx, float Fy, float dt){
  int i = 0, j = 0, k = 0;

  for(i=0; i<NX; i++){
    for(j=0; j<NY-2; j++){ //Nunca ocorrerá a colisão na interface que separa as duas malhas
      for(k=0; k<NVEL; k++){
        feq[i][j][k] = w[k] * rho[i][j] * (1.0 + 3.0 * (cx[k] * u[i][j] + cy[k] * v[i][j]) + 4.5 * (cx[k] * u[i][j] + cy[k] * v[i][j]) * (cx[k] * u[i][j] + cy[k] * v[i][j]) - 1.5 * (u[i][j] * u[i][j] + v[i][j] * v[i][j]));

        F[i][j][k] = (1.0 - (0.5 * dt)/tau) * w[k] * ((3.0 * (cx[k] - u[i][j]) + 9.0 * (cx[k] * u[i][j] + cy[k] * v[i][j]) * cx[k]) * Fx + (3.0 * (cy[k] - v[i][j]) + 9.0 * (cx[k] * u[i][j] + cy[k] * v[i][j]) * cy[k]) * Fy);

        f[i][j][k] = (1.0 - (dt/tau)) * fprop[i][j][k] + (dt/tau) * feq[i][j][k] + F[i][j][k]; 
      }
    }
  }
}

void collisionCoarseGrid(int NX, int NY, float tau, float rho[NX][NY], float u[NX][NY], float v[NX][NY],float f[NX][NY][NVEL], float fprop[NX][NY][NVEL], float feq[NX][NY][NVEL], float F[NX][NY][NVEL], int cx[NVEL], int cy[NVEL], float w[NVEL], float Fx, float Fy, float dt){
  int i = 0, j = 0, k = 0;

  for(i=0; i<NX; i++){
    for(j=0; j<NY; j++){
      for(k=0; k<NVEL; k++){
        feq[i][j][k] = w[k] * rho[i][j] * (1.0 + 3.0 * (cx[k] * u[i][j] + cy[k] * v[i][j]) + 4.5 * (cx[k] * u[i][j] + cy[k] * v[i][j]) * (cx[k] * u[i][j] + cy[k] * v[i][j]) - 1.5 * (u[i][j] * u[i][j] + v[i][j] * v[i][j]));

        F[i][j][k] = (1.0 - (0.5 * dt)/tau) * w[k] * ((3.0 * (cx[k] - u[i][j]) + 9.0 * (cx[k] * u[i][j] + cy[k] * v[i][j]) * cx[k]) * Fx + (3.0 * (cy[k] - v[i][j]) + 9.0 * (cx[k] * u[i][j] + cy[k] * v[i][j]) * cy[k]) * Fy);

        f[i][j][k] = (1.0 - (dt/tau)) * fprop[i][j][k] + (dt/tau) * feq[i][j][k] + F[i][j][k]; 
      }
    }
  }
}

void explode(int NX_f, int NY_f, int NX_c, int NY_c, int cx[NVEL], int cy[NVEL], float f_f[NX_f][NY_f][NVEL], float f_c[NX_c][NY_c][NVEL]){
  int cont, i = 0, k = 0;
  for(k=0;k<NVEL;k++){
    cont = 0;
    for(i=0;i<NX_f;i+=2){
    f_f[i][NY_f-1][k] = f_c[cont][0][k] * 0.25;
    f_f[i+1][NY_f-1][k] = f_c[cont][0][k] * 0.25;
    f_f[i][NY_f-2][k] = f_c[cont][0][k] * 0.25;
    f_f[i+1][NY_f-2][k] = f_c[cont][0][k] * 0.25;
    cont++; 
    } 
  } 
}

void streamingFineGrid(int NX, int NY, int cx[NVEL], int cy[NVEL], float f[NX][NY][NVEL], float fprop[NX][NY][NVEL]){
  int i, j, k;
  int novox, novoy; 
  for(i=1;i<=NX;i++){
    for(j=1;j<=NY;j++){
      for(k=0;k<NVEL;k++){
        novox = 1 + ((i-1 + cx[k] + NX) % NX);
        novoy = 1 + ((j-1 + cy[k] + NY) % NY);
        
        if((j == 1 && novoy == NY) || (j == NY && novoy == 1)){
          continue;
        }else{
          fprop[novox-1][novoy-1][k] = f[i-1][j-1][k];
        }
      }
    }
  }
}


void streamingCoarseGrid(int NX, int NY, int cx[NVEL], int cy[NVEL], float f[NX][NY][NVEL], float fprop[NX][NY][NVEL]){
  int i, j, k;
  int novox, novoy; 

  for(i=1;i<=NX;i++){
    for(j=1;j<=NY;j++){
      for(k=0;k<NVEL;k++){
        novox = 1 + ((i-1 + cx[k] + NX) % NX);
        novoy = 1 + ((j-1 + cy[k] + NY) % NY);
        
        if((j == 1 && novoy == NY) || (j == NY && novoy == 1)){
          continue; //aqui nesses espaços ele não propaga
        }else{
          fprop[novox-1][novoy-1][k] = f[i-1][j-1][k];
        }
      }
    }
  }
}

void bounceBackTopWall(int NX, int NY, float f[NX][NY][NVEL], float fprop[NX][NY][NVEL]){
  int i = 0;
  for(i=0; i<NX; i++){
    fprop[i][NY-1][3] = f[i][NY-1][1];
    fprop[i][NY-1][6] = f[i][NY-1][4];
    fprop[i][NY-1][7] = f[i][NY-1][5];
  }
}

void bounceBackBottomWall(int NX, int NY, float f[NX][NY][NVEL], float fprop[NX][NY][NVEL]){
  int i = 0;
  for(i=0; i<NX; i++){
    fprop[i][0][1] = f[i][0][3];
    fprop[i][0][4] = f[i][0][6];
    fprop[i][0][5] = f[i][0][7];
  }
}

void coalesce(int NX_f, int NY_f, int NX_c, int NY_c, float fprop_f[NX_f][NY_f][NVEL], float fprop_c[NX_c][NY_c][NVEL]){
  int k = 0, i = 0;
  for(k=0;k<NVEL;k++){
    if(k == 1 || k == 4 || k ==5){
      for(i=0;i<NX_c;i++){
        fprop_c[i][0][k] = fprop_f[(int)(2*i)][NY_f-1][k] + fprop_f[(int)(2*i)+1][NY_f-1][k] + fprop_f[(int)(2*i)][NY_f-2][k] + fprop_f[(int)(2*i)+1][NY_f-2][k];
      }
    } 
  }
}



int main(){
  int i = 0, j = 0, k = 0, t = 0;

  float dt_c = 1.;

  float dt_f = dt_c;

  //PARÂMETROS DA SIMULAÇÃO

  int scale = 2; //nível de refino
  int NX_c = 5; //comprimento do canal (length)
  int NY_c = 4; //largura do canal (width)
  int NX_f = NX_c * scale; 
  int NY_f = (NY_c * scale) + scale; 
  int NSTEPS = 1E4; //number of time steps
  float tau_f, tau_c, omega_f, omega_c, u_max, nu_f, nu_c, Re_f, Re_c;
  nu_f = 1.0/3.0; //kinematic viscosity
  nu_c = 1.0/6.0;
  tau_f = (6.0 * nu_f + 1.0)/2.0; //relaxation time
  tau_c = (6.0 * nu_c + 1.0)/2.0;
  omega_f = 1 / tau_f; //relaxation frequency 
  omega_c = 1 / tau_c;
  u_max = 0.1; //centerline velocity (maximum velocity)
  Re_f = ((NY_f-2) * 2) * u_max / nu_f; //Reynolds number
  Re_c = (NY_c * 2) * u_max / nu_c;
  printf("\nFine grid parameters: \ntau = %f \nomega = %f \nnu = %f \nRe = %f \n", tau_f, omega_f, nu_f, Re_f);
  printf("\nCoarse grid parameters: \ntau = %f \nomega = %f \nnu = %f \nRe = %f \n", tau_c, omega_c, nu_c, Re_c);


  //PARÂMETROS DA MALHA D2Q9 - note que a direção zero é a última

  float w[NVEL]; //pesos
  int cx[NVEL]; //componente x das velocidades
  int cy[NVEL]; //componente y das velocidades
  w[0] = 1./9.;
  w[1] = 1./9.;
  w[2] = 1./9.;
  w[3] = 1./9.;
  w[4] = 1./36.;
  w[5] = 1./36.;
  w[6] = 1./36.;
  w[7] = 1./36.;
  w[8] = 4./9.;
  cx[0] = 1;
  cx[1] = 0;
  cx[2] = -1;
  cx[3] = 0;
  cx[4] = 1;
  cx[5] = -1;
  cx[6] = -1;
  cx[7] = 1;
  cx[8] = 0;
  cy[0] = 0;
  cy[1] = 1;
  cy[2] = 0;
  cy[3] = -1;
  cy[4] = 1;
  cy[5] = 1;
  cy[6] = -1;
  cy[7] = -1;
  cy[8] = 0;

  //CONDIÇÕES DE PRESSÃO

  //float gradP_f = 0.0;
  //float gradP_c = 0.0;
  float Fx_f = (0.25)/(960.);
  float Fy_f = 0.;
  float Fx_c = 1./(480.); 
  float Fy_c = 0.; 

  //float rho_outlet_c = 1.0;
  //float rho_inlet_c = 3.0 * (NX_c - 1.0) * gradP_c + rho_outlet_c;

  //float rho_outlet_f = rho_outlet_c * 0.25;
  //float rho_inlet_f = 3.0 * (NX_f - 1.0) * gradP_f + rho_outlet_f;

  //SOLUÇÃO ANALÍTICA
  
  float ybottom = 0.0, ytop = 100, y[100];
  float u_analitico[100];
  FILE *analitico = fopen("analitico.txt", "w+");
  for(j=0;j<100;j++){
    y[j] = j + 0.5;
    u_analitico[j] = -4 * u_max/(100*100) * (y[j] - ybottom) * (y[j] - ytop);
    fprintf(analitico, "%f ", u_analitico[j]);
  }
  fclose(analitico);

    //INICIALIZANDO FUNÇÕES DE DISTRIBUIÇÃO

   float f_f[NX_f][NY_f][NVEL], fprop_f[NX_f][NY_f][NVEL], feq_f[NX_f][NY_f][NVEL], rho_f[NX_f][NY_f], u_f[NX_f][NY_f], v_f[NX_f][NY_f], F_f[NX_f][NY_f][NVEL];
  float f_c[NX_c][NY_c][NVEL], fprop_c[NX_c][NY_c][NVEL], feq_c[NX_c][NY_c][NVEL], rho_c[NX_c][NY_c], u_c[NX_c][NY_c], v_c[NX_c][NY_c], F_c[NX_c][NY_c][NVEL];
  for(i=0;i<NX_f;i++){
    for(j=0;j<NY_f;j++){
      for(k=0;k<NVEL;k++){
        u_f[i][j] = 0.0;
        v_f[i][j] = 0.0;
        rho_f[i][j] = 0.25;
        feq_f[i][j][k] = w[k] * rho_f[i][j] * (1.0 + 3.0 * (cx[k] * u_f[i][j] + cy[k] * v_f[i][j]) + 4.5 * (cx[k] * u_f[i][j] + cy[k] * v_f[i][j]) * (cx[k] * u_f[i][j] + cy[k] * v_f[i][j]) - 1.5 * (u_f[i][j] * u_f[i][j] + v_f[i][j] * v_f[i][j]));

        f_f[i][j][k] = feq_f[i][j][k];
        fprop_f[i][j][k] = feq_f[i][j][k];
        F_f[i][j][k] = 0.0;
      }
    }
  }
  for(i=0;i<NX_c;i++){
    for(j=0;j<NY_c;j++){
      for(k=0;k<NVEL;k++){
        u_c[i][j] = 0.0;
        v_c[i][j] = 0.0;
        rho_c[i][j] = 1.0;
        
        feq_c[i][j][k] = w[k] * rho_c[i][j] * (1.0 + 3.0 * (cx[k] * u_c[i][j] + cy[k] * v_c[i][j]) + 4.5 * (cx[k] * u_c[i][j] + cy[k] * v_c[i][j]) * (cx[k] * u_c[i][j] + cy[k] * v_c[i][j]) - 1.5 * (u_c[i][j] * u_c[i][j] + v_c[i][j] * v_c[i][j]));
        
        f_c[i][j][k] = feq_c[i][j][k];
        fprop_c[i][j][k] = feq_c[i][j][k];
        F_c[i][j][k] = 0.0;
      }
    }
  }

  //MAIN ALGORITHM 
  for(t=0;t<=NSTEPS;t++){
    //Computing x-component velocity, y-component velocity and specific mass 
    macroscopicQuantities(NX_f, NY_f, rho_f, u_f, v_f, fprop_f, cx, cy, Fx_f, Fy_f, dt_f);
    macroscopicQuantities(NX_c, NY_c, rho_c, u_c, v_c, fprop_c, cx, cy, Fx_c, Fy_c, dt_c);
    
    //Colliding all 
    collisionFineGrid(NX_f, NY_f, tau_f, rho_f, u_f, v_f, f_f, fprop_f, feq_f, F_f, cx, cy, w, Fx_f, Fy_f, dt_f);
    collisionCoarseGrid(NX_c, NY_c, tau_c, rho_c, u_c, v_c, f_c, fprop_c, feq_c, F_c, cx, cy, w, Fx_c, Fy_c, dt_c);

    //Explode
    explode(NX_f, NY_f, NX_c, NY_c, cx, cy, f_f, f_c);

    //Streaming all
    streamingFineGrid(NX_f, NY_f, cx, cy, f_f, fprop_f);
    bounceBackBottomWall(NX_f, NY_f, f_f, fprop_f);

    streamingCoarseGrid(NX_c, NY_c, cx, cy, f_c, fprop_c);
    bounceBackTopWall(NX_c, NY_c, f_c, fprop_c);
    
    //Colliding Fine Grid
    collisionFineGrid(NX_f, NY_f, tau_f, rho_f, u_f, v_f, f_f, fprop_f, feq_f, F_f, cx, cy, w, Fx_f, Fy_f, dt_f);   

    //Streaming Fine and interface
    streamingFineGrid(NX_f, NY_f, cx, cy, f_f, fprop_f);
    bounceBackBottomWall(NX_f, NY_f, f_f, fprop_f);

    //Coalescing
    coalesce(NX_f, NY_f, NX_c, NY_c, fprop_f, fprop_c);
  }

  FILE *density_f = fopen("rho_f.txt", "w+");
  FILE *velocityx_f = fopen("u_f.txt", "w+");
  FILE *velocityy_f = fopen("v_f.txt", "w+");
  FILE *density_c = fopen("rho_c.txt", "w+");
  FILE *velocityx_c = fopen("u_c.txt", "w+");
  FILE *velocityy_c = fopen("v_c.txt", "w+");
  
  
  for(i=0;i<NX_f;i++){
    for(j=0;j<NY_f-2;j++){
      fprintf(density_f, "%f ", rho_f[i][j]);
      fprintf(velocityx_f, "%f ", u_f[i][j]);				
      fprintf(velocityy_f, "%f ", v_f[i][j]);
    }
    fprintf(velocityx_f, "\n");
    fprintf(velocityy_f, "\n");
    fprintf(density_f, "\n");
  }   
  for(i=0;i<NX_c;i++){
    for(j=0;j<NY_c;j++){
      fprintf(density_c, "%f ", rho_c[i][j]);
      fprintf(velocityx_c, "%f ", u_c[i][j]);				
      fprintf(velocityy_c, "%f ", v_c[i][j]);
    }
    fprintf(velocityx_c, "\n");
    fprintf(velocityy_c, "\n");
    fprintf(density_c, "\n");
  }

  fclose(velocityx_f);
  fclose(velocityy_f);
  fclose(density_f);
  fclose(velocityx_c);
  fclose(velocityy_c);
  fclose(density_c);
  return 0;
}

