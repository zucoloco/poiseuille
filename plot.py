#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 13:24:05 2020

@author: gustavo
"""

import numpy as np
import matplotlib.pylab as plt 

#SOLUÇÃO ANALÍTICA
solucao_analitica = np.loadtxt("analitico.txt")

#COLETANDO DADOS DA MALHA GROSSEIRA
u8 = np.loadtxt("u8.txt")
v8 = np.loadtxt("v8.txt")

NX8, NY8 = np.shape(u8)

adimensional8 = np.zeros(NY8)

for i in range(0, NY8):   
   adimensional8[i] = (i+0.5) / NY8

#COLETANDO DADOS DA MALHA REFINADA
u16 = np.loadtxt("u16.txt")
v16 = np.loadtxt("v16.txt")

NX16, NY16 = np.shape(u16)

adimensional16 = np.zeros(NY16)

for i in range(0, NY16):   
   adimensional16[i] = (i+0.5) / NY16
   
#COLETANDO OS DADOS DA MALHA MISTA
   
u_f = np.loadtxt("u_f.txt")
v_f = np.loadtxt("v_f.txt")
u_c = np.loadtxt("u_c.txt")
v_c = np.loadtxt("v_c.txt")


plt.plot(np.linspace(0, 1, 100), solucao_analitica)
plt.plot(adimensional8, u8[2], 'v')
plt.plot(adimensional16, u16[2], '^')
plt.plot(np.linspace(0.025, 0.475, 8), u_f[2], 'ro')
plt.plot(np.linspace(0.55, 0.95, 4), u_c[2], 'ro')


plt.ylabel('u')
plt.xlabel('y/L')
plt.legend(['Solução Analítica', 'Malha Grosseira de 8 pontos (nu = 1/6)', 'Malha Refinada de 16 pontos (nu = 1/3)', 'Refinada na parte inferior e Grosseira na Superior', 'Refinada na parte inferior e Grosseira na Superior'], loc=8)
plt.show()
