#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define NVEL 9

void macroscopicQuantities(int NX, int NY, float rho[NX][NY], float u[NX][NY], float v[NX][NY], float fprop[NX][NY][NVEL], int cx[NVEL], int cy[NVEL], float Fx, float Fy, float dt){
  int i = 0, j = 0, k = 0;
  float soma = 0., somau = 0., somav = 0.;
  for(j=0;j<NY;j++){
    for(i=0;i<NX;i++){
      soma = 0.0;
      for(k=0;k<NVEL;k++){
        soma = soma + fprop[i][j][k];
      }
      rho[i][j] = soma; //densidade
    }
  }

  for(j=0;j<NY;j++){
    for(i=0;i<NX;i++){
      somau = 0.0;
      somav = 0.0;
      for(k=0;k<NVEL;k++){
          somau = somau + fprop[i][j][k] * cx[k];
          somav = somav + fprop[i][j][k] * cy[k];
      }
      u[i][j] = (somau + 0.5 * Fx * dt) / rho[i][j]; //componente x da velocidade
      v[i][j] = (somav + 0.5 * Fy * dt) / rho[i][j]; //componente y da velocidade 
    }
  }
}

void collision(int NX, int NY, float tau, float rho[NX][NY], float u[NX][NY], float v[NX][NY],float f[NX][NY][NVEL], float fprop[NX][NY][NVEL], float feq[NX][NY][NVEL], float F[NX][NY][NVEL], int cx[NVEL], int cy[NVEL], float w[NVEL], float Fx, float Fy, float dt){
  int i = 0, j = 0, k = 0;

  for(i=0; i<NX; i++){
    for(j=0; j<NY; j++){
      for(k=0; k<NVEL; k++){
        feq[i][j][k] = w[k] * rho[i][j] * (1.0 + 3.0 * (cx[k] * u[i][j] + cy[k] * v[i][j]) + 4.5 * (cx[k] * u[i][j] + cy[k] * v[i][j]) * (cx[k] * u[i][j] + cy[k] * v[i][j]) - 1.5 * (u[i][j] * u[i][j] + v[i][j] * v[i][j]));

        F[i][j][k] = (1.0 - (0.5 * dt)/tau) * w[k] * ((3.0 * (cx[k] - u[i][j]) + 9.0 * (cx[k] * u[i][j] + cy[k] * v[i][j]) * cx[k]) * Fx + (3.0 * (cy[k] - v[i][j]) + 9.0 * (cx[k] * u[i][j] + cy[k] * v[i][j]) * cy[k]) * Fy);

        f[i][j][k] = (1.0 - (dt/tau)) * fprop[i][j][k] + (dt/tau) * feq[i][j][k] + F[i][j][k]; 
      }
    }
  }
}

void streaming(int NX, int NY, int cx[NVEL], int cy[NVEL], float f[NX][NY][NVEL], float fprop[NX][NY][NVEL]){
  int i, j, k;
  int novox, novoy; 
  for(i=1;i<=NX;i++){
    for(j=1;j<=NY;j++){
      for(k=0;k<NVEL;k++){
        novox = 1 + ((i-1 + cx[k] + NX) % NX);
        novoy = 1 + ((j-1 + cy[k] + NY) % NY);
        
        if((j == 1 && novoy == NY) || (j == NY && novoy == 1)){
          continue;
        }else{
          fprop[novox-1][novoy-1][k] = f[i-1][j-1][k];
        }
      }
    }
  }
}

void bounceBackTopWall(int NX, int NY, float f[NX][NY][NVEL], float fprop[NX][NY][NVEL]){
  int i = 0;
  for(i=0; i<NX; i++){
    fprop[i][NY-1][3] = f[i][NY-1][1];
    fprop[i][NY-1][6] = f[i][NY-1][4];
    fprop[i][NY-1][7] = f[i][NY-1][5];
  }
}

void bounceBackBottomWall(int NX, int NY, float f[NX][NY][NVEL], float fprop[NX][NY][NVEL]){
  int i = 0;
  for(i=0; i<NX; i++){
    fprop[i][0][1] = f[i][0][3];
    fprop[i][0][4] = f[i][0][6];
    fprop[i][0][5] = f[i][0][7];
  }
}

int main(){
  int i = 0, j = 0, k = 0, t = 0;
  
  //SIMULATION PARAMETERS
  float dt = 1.0;
  int NX = 5; //channel length
  int NY = 8; //channel width
  int NSTEPS = 1E4; //number of time steps
  float tau, omega, u_max, nu, Re;
  nu = 1.0/6.0; //kinematic viscosity
  tau = (6.0 * nu + 1.0)/2.0; //relaxation time
  omega = 1 / tau; //relaxation frequency
  u_max = 0.1; //centerline velocity (maximum velocity)
  Re = NY * u_max / nu; //Reynolds number
  
  printf("\nSimulation Parameters: tau = %f    omega = %f    nu = %f     Re = %f     \n", tau, omega, nu, Re);
  
  
  //D2Q9 GRID PARAMETERS (note that the direction 0 - the one that doesn't propagate - is the last one)
  float w[NVEL]; //weights
  int cx[NVEL]; //x-component 
  int cy[NVEL]; //y-component 
  w[0] = 1./9.;
  w[1] = 1./9.;
  w[2] = 1./9.;
  w[3] = 1./9.;
  w[4] = 1./36.;
  w[5] = 1./36.;
  w[6] = 1./36.;
  w[7] = 1./36.;
  w[8] = 4./9.;
  cx[0] = 1;
  cx[1] = 0;
  cx[2] = -1;
  cx[3] = 0;
  cx[4] = 1;
  cx[5] = -1;
  cx[6] = -1;
  cx[7] = 1;
  cx[8] = 0;
  cy[0] = 0;
  cy[1] = 1;
  cy[2] = 0;
  cy[3] = -1;
  cy[4] = 1;
  cy[5] = 1;
  cy[6] = -1;
  cy[7] = -1;
  cy[8] = 0;
  
  //Setting the Pressure Gradient and the Force
  float gradP = 0.0;
  float Fx = 1./480;
  float Fy = 0.;
    
  //SOLUÇÃO ANALÍTICA
  float ybottom = 0.0, ytop = 100, y[100];
  float u_analitico[100];
  FILE *analitico = fopen("analitico.txt", "w+");
  
  for(j=0;j<100;j++){
      y[j] = j + 0.5;
      u_analitico[j] = -4 * u_max/(100*100) * (y[j] - ybottom) * (y[j] - ytop);
      fprintf(analitico, "%f ", u_analitico[j]);
  }
  
  fclose(analitico);
    

    
  //INICIALIZANDO FUNÇÕES DE DISTRIBUIÇÃO0
  float f[NX][NY][NVEL], fprop[NX][NY][NVEL], feq[NX][NY][NVEL], rho[NX][NY], u[NX][NY], v[NX][NY], F[NX][NY][NVEL];
  for(i=0;i<NX;i++){
    for(j=0;j<NY;j++){
      for(k=0;k<NVEL;k++){
        u[i][j] = 0.0;
        v[i][j] = 0.0;
        rho[i][j] = 1.0;
        
        feq[i][j][k] = w[k] * rho[i][j] * (1.0 + 3.0 * (cx[k] * u[i][j] + cy[k] * v[i][j]) + 4.5 * (cx[k] * u[i][j] + cy[k] * v[i][j]) * (cx[k] * u[i][j] + cy[k] * v[i][j]) - 1.5 * (u[i][j] * u[i][j] + v[i][j] * v[i][j]));

        f[i][j][k] = feq[i][j][k];
        fprop[i][j][k] = feq[i][j][k];
        F[i][j][k] = 0.0;
        }
    }
  }
   
    
    
  //ALGORITMO PRINCIPAL
  for(t=0;t<NSTEPS;t++){
    macroscopicQuantities(NX, NY, rho, u, v, fprop, cx, cy, Fx, Fy, dt);

    collision(NX, NY, tau, rho, u, v, f, fprop, feq, F, cx, cy, w, Fx, Fy, dt);

    streaming(NX, NY, cx, cy, f, fprop);

    bounceBackTopWall(NX, NY, f, fprop);
    bounceBackBottomWall(NX, NY, f, fprop);

  }

    

    FILE *densidade = fopen("rho8.txt", "w+");
    FILE *velocidadex = fopen("u8.txt", "w+");
    FILE *velocidadey = fopen("v8.txt", "w+");
    
    for(i=0;i<NX;i++){
        for(j=0;j<NY;j++){
            fprintf(densidade, "%f ", rho[i][j]);
            fprintf(velocidadex, "%f ", u[i][j]);				
            fprintf(velocidadey, "%f ", v[i][j]);
        }
        fprintf(velocidadex, "\n");
        fprintf(velocidadey, "\n");
        fprintf(densidade, "\n");
    }   

    fclose(velocidadex);
    fclose(velocidadey);
    fclose(densidade);
    
    return 0;
}

